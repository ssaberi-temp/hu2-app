/**
 * Load the App component.
 */

import { AppRegistry } from 'react-native';
import AppContainer from './src/';

AppRegistry.registerComponent('HumpHuggers', () => AppContainer);
