/**
 * Global App Config
 */
/* global __DEV__ */

export default {
  // App Details
  appName: 'Hump Huggers',

  // Build Configuration - eg. Debug or Release?
  DEV: __DEV__,

  pageSize: 10,
};
