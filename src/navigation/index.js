import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator, NavigationActions } from 'react-navigation';

import PositionsBrowse from '@containers/positions/Browse/BrowseContainer';
import PositionView from '@containers/positions/Detail/DetailContainer';

export const AppNavigator = StackNavigator(
  {
    PositionsBrowse: { screen: PositionsBrowse },
    PositionView: {
      screen: PositionView,
      path: 'position/:postId',
    },
  },
  {
    initialRouteName: 'PositionsBrowse',
    headerMode: 'Hump Huggers',
  },
);

class AppWithNavigationState extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    const { dispatch, navigation } = this.props;
    if (navigation.index === 0) {
      return false;
    }
    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { dispatch, navigation } = this.props;
    const navigationHelper = addNavigationHelpers({
      dispatch,
      state: navigation,
    });

    return <AppNavigator navigation={navigationHelper} />;
  }
}

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  navigation: state.navigation,
});

export default connect(mapStateToProps)(AppWithNavigationState);
