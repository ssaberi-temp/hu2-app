/* @flow */

import React from "react";
import { DrawerNavigator } from "react-navigation";

import Home from "@containers/drawerhome/";
import SideBar from "@containers/drawer/sidebar";
import PositionsBrowse from '@containers/positions/Browse/BrowseContainer';
import ForumMain from '@containers/forum/Browse/ForumMainContainer';
import KnowledgeBase from '@containers/knowledge/MainCenter/KnowledgeBaseContainer';
import TicketBrowse from '@containers/tickets/Browse/TicketBrowseContainer';
import UserProfile from '@containers/user/Profile/UserProfileContainer';
import FeedbackForm from '@containers/forms/Feedback/FeedbackForm';

const DrawerNav = DrawerNavigator(
  {
    Home: { screen: Home },
    PositionsBrowse: { screen: PositionsBrowse },
    KnowledgeBase: { screen: KnowledgeBase },
    TicketBrowse: { screen: TicketBrowse },
    UserProfile: { screen: UserProfile },
    FeedbackForm: { screen: FeedbackForm },
  },
  {
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

export default DrawerExample;
