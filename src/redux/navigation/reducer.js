/**
 * Navigations Reducer
 */

import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '@navigation/';
import { initialState } from '@redux/humphuggers/reducer';

const initialNavState = AppNavigator.router.getStateForAction(NavigationActions.navigate({ routeName: 'PositionsBrowse' }));

export default function navigationReducer(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case 'Back':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.back(),
        state,
      );
      break;
    case 'PositinView':
      const position = initialState.initialPositions.edges.find(p => p.node.postId === action.params.postId);
      if (position) {
        nextState = AppNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'PositionView', params: { position: position.node } }),
          state,
        );
      } else {
        nextState = AppNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'PositionsBrowse' }),
          state,
        );
      }
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
