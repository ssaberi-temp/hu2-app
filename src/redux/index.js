/**
 * Combine All Reducers
 */

// Our custom reducers
import humphuggers from '@redux/humphuggers/reducer';
import navigation from '@redux/navigation/reducer';

// Combine all
const appReducer = {
  humphuggers,
  navigation,
};

export default appReducer;
