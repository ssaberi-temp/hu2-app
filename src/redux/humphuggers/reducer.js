/**
 * HumpHuggers Reducer
 */

/**
 * 'difficulty' indicates the difficulty of each position and
   also reflecs on the icon of each position.
   There are 3 levels of difficulties:
   1. easy: 'chili-mild'
   2. medium: 'chili-medium'
   3. hard: 'chili-hot'
 */

// Set initial state
export const initialState = {
  categories: [
    {
      id: 16,
      name: 'Positions',
      type: 'parent',
    },
  ],
  initialPositions: {
    edges: [
      {
        node: {
          postId: 111,
          title: 'Best Age',
          category: 'Category 1',
          difficulty: 'chili-hot',
          content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
          excerpt: 'Bes t age excerpt',
          featuredImage: {
            sourceUrl: require('@assets/positions/pos1.gif'),
            type: 'local',
          },
          tags: {
            edges: [],
          },
        },
      },
      {
        node: {
          postId: 112,
          title: 'Best Age',
          category: 'Category 2',
          difficulty: 'chili-mild',
          content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
          excerpt: 'Bes t age excerpt',
          featuredImage: {
            sourceUrl: require('@assets/positions/pos2.gif'),
            type: 'local',
          },
          tags: {
            edges: [],
          },
        },
      },
      {
        node: {
          postId: 113,
          title: 'Best Age',
          category: 'Category 2',
          difficulty: 'chili-medium',
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
          excerpt: 'Best age excerpt',
          featuredImage: {
            sourceUrl: require('@assets/positions/pos3.gif'),
            type: 'local',
          },
          tags: {
            edges: [],
          },
        },
      },
      {
        node: {
          postId: 114,
          title: 'XYZ sdb',
          category: 'Category 3',
          difficulty: 'chili-mild',
          content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
          excerpt: 'hand job excerpt',
          featuredImage: {
            sourceUrl: require('@assets/positions/pos4.gif'),
            type: 'local',
          },
          tags: {
            edges: [],
          },
        },
      },
    ],
  },
};

export default function humphuggersReducer(state = initialState, action) {
  return state;
}
