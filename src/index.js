/**
 * Index - this is where everything starts
 */

/* global __DEV__ */
import React, { Component } from 'react';
import { Platform } from 'react-native';
import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';

import { Root } from 'native-base';

// Consts and Libs
import AppWithNavigationState from '@navigation/';
import getTheme from '@theme/components';

// All redux reducers (rolled into one mega-reducer)
import rootReducer from '@redux/index';

// SplashScreen
import SplashScreen from 'react-native-splash-screen';

// Load middleware
let middleware = [];

if (__DEV__) {
  // Dev-only middleware
  middleware = [
    ...middleware,
    createLogger(), // Logs state changes to the dev console
  ];
}

const appReducer = combineReducers({
  ...rootReducer,
});

// on Android, the URI prefix typically contains a host in addition to scheme
const prefix = Platform.OS === 'android' ? 'humphuggers://humphuggers/' : 'humphuggers://';

// Init redux store (using the given reducer & middleware)
const store = compose(applyMiddleware(...middleware))(createStore)(appReducer);

/* Component ==================================================================== */
// Wrap App in Redux provider (makes Redux available to all sub-components)
// export default function AppContainer() {
export default class AppContainer extends Component<{}> {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <Provider store={store} style={getTheme()}>
        <Root>
          <AppWithNavigationState uriPrefix={prefix} />
        </Root>
      </Provider>
    );
  }
}
