/**
 * Main Tabs Container
 */
import { connect } from 'react-redux';

// The component we're mapping to
import PositionsBrowseView from './BrowseView';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  categories: state.humphuggers.categories || [],
  initialPositions: state.humphuggers.initialPositions || [],
});

// Any actions to map to the component?
const mapDispatchToProps = dispatch => ({
  goToPosition(positionId) {
    dispatch({ type: 'PositinView', params: { postId: positionId } });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PositionsBrowseView);
