/**
 * Main Positions View
 */
import React, { Component } from 'react';
import { Platform, Linking } from 'react-native';
import PropTypes from 'prop-types';

// Containers
import PositionsListContainer from '@containers/positions/List/ListView';

/* Component ==================================================================== */
class PositionsBrowseView extends Component {
  componentDidMount() {
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then((url) => {
        this.navigate(url);
      });
    } else {
      Linking.addEventListener('url', this.handleOpenURL.bind(this));
    }
  }
  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
  }
    handleOpenURL = (event) => {
      this.navigate(event.url);
    }

    navigate(url) {
      const { goToPosition } = this.props;
      const route = url.replace(/.*?:\/\//g, '');
      const id = route.match(/\/([^\/]+)\/?$/)[1];
      const routeName = route.replace('humphuggers/', '').split('/')[0];

      if (routeName === 'position') {
        goToPosition(parseInt(id));
      }
    }

    render = () => {
      const { initialPositions } = this.props;
      return (
        <PositionsListContainer
          initialPositions={initialPositions}
        />
      );
    }
}

PositionsBrowseView.componentName = 'PositionsBrowseView';

PositionsBrowseView.propTypes = {
  initialPositions: PropTypes.shape({
    edges: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
  goToPosition: PropTypes.func.isRequired,
};


/* Export Component ==================================================================== */
export default PositionsBrowseView;
