/**
 * Main Header View
 */
import React, { PureComponent } from 'react';
import { Image, View } from 'react-native';
import {
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Text,
} from 'native-base';
import styles from '@containers/positions/styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

/* Component ==================================================================== */
class MainHeaderView extends PureComponent {

  render = () => {
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => null} >
            <Icon name="menu" style={styles.headerIcons} />
          </Button>
        </Left>
        <Body>
          <Title>Header</Title>
        </Body>
        <Right>
          <Button transparent>
            <Icon name="magnify" style={styles.headerIcons} />
          </Button>
          <Button transparent>
            <Icon name="wunderlist" style={styles.headerIcons} />
          </Button>
        </Right>
      </Header>
    );
  }
}

MainHeaderView.componentName = 'MainHeaderView';

/* Export Component ==================================================================== */
export default MainHeaderView;
