export default {
  container: {
    backgroundColor: '#FFF',
  },
  text: {
    alignSelf: 'center',
    marginBottom: 7,
  },
  mb: {
    marginBottom: 15,
  },
  headerIcons: {
    color: '#fff',
    fontSize: 36,
  },
  iconTopRight: {
    position: 'absolute',
    right: 15,
    top: 0,
    zIndex: 9999
  },
  iconBottomLeft: {
    position: 'absolute',
    right: 15,
    bottom: 0,
    zIndex: 9999
  },
};
