/**
 * Position Detail View Screen
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  View,
} from 'react-native';
import firebase from 'react-native-firebase';

import {
  Container,
  Header,
  Footer,
  Title,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Card,
  CardItem,
} from 'native-base';

import styles from '@containers/positions/styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

/* Component ==================================================================== */
class PositionDetailView extends PureComponent {
  render = () => {
    const { goBack, navigation } = this.props;
    const position = navigation.state.params.position;
    const { title, content, featuredImage, category } = position;

    const Banner = firebase.admob.Banner;
    const NativeExpress = firebase.admob.NativeExpress;
    const AdRequest = firebase.admob.AdRequest;
    const request = new AdRequest();
    request.addKeyword('foobar');


    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" style={styles.headerIcons} />
            </Button>
          </Left>
          <Body style={{ flexDirection: 'row' }}>
            <Icon name={position.difficulty} size={24} color="#FFFFFF" />
            <Title>{title}</Title>
          </Body>
          <Right>
            <View style={{ flexDirection: 'row' }}>
              <Button transparent onPress={() => null} >
                <Icon name="heart-outline" size={24} color="#4F8EF7" />
              </Button>
              <Button transparent onPress={() => null} >
                <Icon name="thumb-up-outline" size={24} color="#4F8EF7" />
              </Button>
              <Button transparent onPress={() => null }>
                <Icon name="share-variant" size={24} color="#4F8EF7" />
              </Button>
            </View>
          </Right>
        </Header>

        <Content padder>
          <Card>
            <CardItem cardBody>
              <Image
                style={{
                      resizeMode: 'cover',
                      width: null,
                      height: 200,
                      flex: 1,
                    }}
                source={featuredImage.sourceUrl}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Icon name={position.difficulty} size={32} color="#4F8EF7" />
                <Body>
                  <Text>{title}</Text>
                  <Text note>{category}</Text>
                </Body>
              </Left>
              <Right>
                <View style={{ flexDirection: 'row' }}>
                  <Button transparent style={{ marginRight: 15 }} onPress={() => null} >
                    <Icon name="heart-outline" size={24} color="#4F8EF7" />
                  </Button>
                  <Button transparent style={{ marginRight: 15 }} onPress={() => null} >
                    <Icon name="thumb-up-outline" size={24} color="#4F8EF7" />
                  </Button>
                  <Button transparent onPress={() => null }>
                    <Icon name="share-variant" size={24} color="#4F8EF7" />
                  </Button>
                </View>
              </Right>
            </CardItem>
            <CardItem>
              <Text style={{ textAlign: 'justify'}}>
                {content}
              </Text>
            </CardItem>
            <CardItem>

            </CardItem>
          </Card>
        </Content>
        <Footer>
          <Body>
            <Banner
              unitId="ca-app-pub-3940256099942544/6300978111"
              size="FULL_BANNER"
              request={request.build()}
            />
          </Body>
        </Footer>
      </Container>
    );
  }
}

PositionDetailView.componentName = 'PositionDetailView';

PositionDetailView.propTypes = {
  goBack: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    state: PropTypes.shape({
      params: PropTypes.shape({
        position: PropTypes.object.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

/* Export Component ==================================================================== */
export default PositionDetailView;
