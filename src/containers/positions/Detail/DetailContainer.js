/**
 * Individual Position Detail Container
 */
import { connect } from 'react-redux';

// Components
import PositionDetailView from './DetailView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
});

// Any actions to map to the component?
const mapDispatchToProps = dispatch => ({
  goBack: () => dispatch({ type: 'Back' }),
});

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(PositionDetailView);
