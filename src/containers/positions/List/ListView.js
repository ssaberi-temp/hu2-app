import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, Text } from 'react-native';
import {
  Container,
  Content,
} from 'native-base';

// Containers
import PositionCard from '@containers/positions/Card/CardContainer';
import MainHeaderView from '@containers/positions/Header/MainHeaderView';

import styles from '@containers/positions/styles';

/* Component ==================================================================== */
class PositionsListView extends Component {
    keyExtractor = item => item.node.postId;

    renderItem = position => <PositionCard position={position.item.node} />;

    render = () => {
      const { initialPositions } = this.props;

      return (
        <Container style={styles.container}>
          <MainHeaderView />

          <Content padder>
            <FlatList
              data={initialPositions.edges}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
          </Content>
        </Container>
      );
    }
}

PositionsListView.componentName = 'PositionsListView';

PositionsListView.propTypes = {
  initialPositions: PropTypes.shape({
    edges: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
};

/* Export Component ==================================================================== */
export default PositionsListView;
