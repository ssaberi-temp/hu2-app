/**
 * Test to check if the container is created correctly
 */
/* global it expect */
import 'react-native';

import PositionsList from '@containers/positions/PositionsList';

// Check if PositionsList is created correctly
it('PositionsList is created correctly', () => {
  expect(typeof PositionsList).toEqual('function');
});
