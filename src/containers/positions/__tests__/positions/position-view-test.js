/**
 * Test to check if the container is created correctly
 */
/* global it expect */
import 'react-native';

import PositionView from '@containers/positions/PositionView';

// Check if PositionView is created correctly
it('PositionView is created correctly', () => {
  expect(typeof PositionView).toEqual('function');
});
