/**
 * Position Card View
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';
import {
  Button,
  Content,
  Card,
  CardItem,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import { DrawerNavigator } from "react-navigation";
import firebase from 'react-native-firebase';
import styles from '@containers/positions/styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

/**
 * Icon names
   Favorite: heart/heart-outline || star/start-outline
   like: thump-up/thumbs-up-outline
   camera numbers: numeric-4-box-multiple-outline
 */

/* Component ==================================================================== */
class PositionCardView extends PureComponent {
  render = () => {
    const {
      position, onPressCard,
    } = this.props;

    const NativeExpress = firebase.admob.NativeExpress;
    const AdRequest = firebase.admob.AdRequest;
    const request = new AdRequest();
    request.addKeyword('foobar');

    return (
      <View>
        <Card style={styles.mb}>
          <CardItem cardBody>
            <Button transparent style={styles.iconTopRight} onPress={() => null} >
              <Icon name="star-outline" size={24} color="#4F8EF7" />
            </Button>
            <Image
              style={{
                resizeMode: 'cover',
                width: null,
                height: 200,
                flex: 1,
              }}
              source={position.featuredImage.sourceUrl}
            />
            <Button transparent onPress={() => onPressCard(position)} style={styles.iconBottomLeft}>
              <Icon name="numeric-4-box-multiple-outline" size={24} color="#4F8EF7" />
            </Button>
          </CardItem>
          <CardItem>
            <Left>
              <Icon name={position.difficulty} size={32} color="#4F8EF7" />
              <Body>
                <Text>{position.title}</Text>
                <Text note>{position.category}</Text>
              </Body>
            </Left>
            <Right>
              <View style={{ flexDirection: 'row' }}>
                <Button transparent style={{ marginRight: 15 }} onPress={() => null} >
                  <Icon name="thumb-up-outline" size={24} color="#4F8EF7" />
                </Button>
                <Button transparent onPress={() => null }>
                  <Icon name="share-variant" size={24} color="#4F8EF7" />
                </Button>
              </View>
            </Right>
          </CardItem>
        </Card>

        {/*
          * Displaying Native Ad
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <NativeExpress
                  unitId="ca-app-pub-3940256099942544/2177258514"
                  size="MEDIUM_RECTANGLE"
                  request={request.build()}
                />
              </Body>
            </CardItem>
          </Card>
        */}
      </View>
    );
  }
}

PositionCardView.componentName = 'PositionCardView';

PositionCardView.propTypes = {
  position: PropTypes.shape({
    title: PropTypes.string,
    category: PropTypes.string,
    difficulty: PropTypes.string,
    content: PropTypes.string,
    excerpt: PropTypes.string,
    featuredImage: PropTypes.object,
  }).isRequired,
  onPressCard: PropTypes.func.isRequired,
};

/* Export Component ==================================================================== */
export default PositionCardView;
