/**
 * Individual Position Card Container
 */
import { connect } from 'react-redux';

// Components
import PositionCardView from './CardView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
});

// Any actions to map to the component?
const mapDispatchToProps = dispatch => ({
  onPressCard(position) {
    dispatch({ type: 'PositinView', params: { postId: position.postId } });
  },
});

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(PositionCardView);
